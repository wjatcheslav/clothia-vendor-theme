//= template/header.php
<header class="page__inventory__header">
    <div class="container">
        <div class="row">
            <div class="small-12 column">
                <div class="page__inventory__header__logo">
                    <a href="<?php bloginfo('wpurl'); ?>/" class="logo">
                        <img width="153" height="40"
                             src="<?php bloginfo('template_url'); ?>/assets/svg/header/logo__inventory_page.svg"
                             alt="Clothia &#8211; Best new designers from Europe">
                    </a>
                    <div class="page__inventory__header__menu-action set_uppercase">
                        <?php
                        wp_nav_menu( array(
                            menu => 'vendor-dashboard-top'
                        ) );
                        ?>
</div>
<div class="page__inventory__header__logout-action set_uppercase">
    <ul id="menu-vendor-dashboard-logout">
        <?php
            global $current_user;
        ?>
        <li><a href="#"><?php echo $current_user->user_login; ?></a></li>
        <li><a class="clear_uppercase logout_link" href="<?php echo wp_logout_url(); ?>">Logout</a></li>
    </ul>
</div>
</div>
<div class="page__inventory__header__bottom">
    <div class="page__inventory__header__bottom__title"><?php wp_title(); ?></div>
    <?php if( is_page('dashboard-media') ): ?>
    <div class="page__inventory__header__bottom__upload">
        <button class="button-primary set_uppercase">Upload new file or image</button>
    </div>
    <?php endif; ?>
    <!-- <form action="<?php echo get_bloginfo('wpurl').'/'.$vendor_dashboard->dashboard_search_page ?>" id="searchform" name="searchform" method="get"> -->
    <div class="page__inventory__header__bottom__search">
        <input id="field_keyword" name="field_keyword" maxlength="30" type="text" class="search" placeholder="Search document or image" />
    </div>
    <!-- </form> -->
</div>
</div>
</div>
</div>
</header>
