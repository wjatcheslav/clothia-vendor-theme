<main class="page__product__main">
    <table>
        <thead class="set_uppercase">
        <tr>
            <th class="col01">
                <input id="product-select-all" type="checkbox"/>
            </th>
            <th class="col02 with_arrow">Name & description<span></span></th>
            <th class="col03"></th>
            <th class="col04 with_arrow">Composition & care<span></span></th>
            <th class="col05">Last note</th>
            <th class="col06">Status</th>
            <th class="col07"></th>
        </tr>
        </thead>
        <tbody  id="product_list">

        <?php
        global $post;
        global $current_user;

        $args = array(
            'post_type' => 'product',
            'taxonomy' => 'yith_shop_vendor',
            'term' => $current_user->user_login,
            //'posts_per_page' => 10,
            'orderby' => 'asc'
        );
        $the_query = new WP_Query($args);
        if ($the_query->have_posts()) : while ($the_query->have_posts()) :
            $the_query->the_post();
            $fields = get_fields();
        ?>
            <tr>
                <td class="col01">
                    <input id="product-select-id-217" data-product-id="217" type="checkbox"/>
                </td>
                <td class="col02" data-label="Product name">
                    <div class="thumb_container">
                        <?php
                        $current_post_id = $post->ID;
                        $thumbnail = get_the_post_thumbnail($current_post_id, '80x80');
                        preg_match('/src="(.*)" class/', $thumbnail, $link);
                        $thumbnail_url = $link[1];
                        if (!empty($link[1])): ?>
                            <img src="<?php echo $thumbnail_url; ?>"/>
                        <?php endif; ?>
                    </div>
                </td>
                <td class="col03">
                    <p>
                        <?php
                        echo $post->post_title;
                        echo '<span class="descr">' . $post->post_content . '</span>';
                        ?>
                    </p>
                </td>
                <td class="col04" data-label="Stock">
                    <table>
                        <tr>
                            <td class="width70" align="left" valign="top"><p class="composition pending">Composition:</p></td>
                            <td align="left" valign="top"><p class="care pending"><?php echo get_field('composition'); ?></p></td>
                        </tr>
                        <tr>
                            <td class="width70" align="left" valign="top"><p class="composition">Care:</p></td>
                            <td align="left" valign="top"><p class="care"><?php echo get_field('care'); ?></p></td>
                        </tr>
                    </table>
                </td>
                <td class="col05 last-note pending">
                    <?php echo get_field('service_information'); ?>
                </td>
                <td class="col06" data-label="Product status">
                    <?php echo my_get_product_status_str(get_field('product_status')); ?>
                </td>
                <td class="col07">
                    <a class="ajax_tb_product button-primary" data-product-id="<?php echo $current_post_id; ?>" href="#"> More details </a>
                </td>
            </tr>
        <?php endwhile; else : ?>
            <p style="text-align: center;">There were no products.</p>
        <?php endif;
        wp_reset_postdata(); ?>
        </tbody>
    </table>
</main>
<div id="tb-product" style="display: none;">
    <div id="tb-product__modal__loading" class="modal__product__loading">Loading...</div>
    <div id="tb-product__modal" class="modal__product" style="display: none;">
        <form name="form_modal_product" id="form_modal_product" action="/?vendor-dashboard" method="post">
        <div class="modal__product__thumb-container alignleft">
            <img class="modal__product__thumb-container__thumbnail" alt="Product thumbnail" src="#" />
        </div>
        <div class="modal__product__input-container alignright">
            <div class="clearfloat">
                <div class="modal__product__input-container__descr"></div>
            </div>
        </div>
        </form>
        <div class="modal__product__diment"></div>
        <div class="modal__product__btn alignleft"></div>
    </div>
</div>