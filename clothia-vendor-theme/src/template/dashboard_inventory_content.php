<main class="page__inventory__main">
    <table>
        <thead class="set_uppercase">
        <tr>
            <th class="col01">
                <input id="product-select-all" type="checkbox"/>
            </th>
            <th class="col02 with_arrow">Product name<span></span></th>
            <th class="col03"></th>
            <th class="col04">Stock</th>
            <th class="col05"></th>
            <th class="col06">Product status</th>
        </tr>
        </thead>
        <tbody id="inventory_list">

        <?php
        global $post;
        global $current_user;

        $args = array(
            'post_type' => 'product',
            'taxonomy' => 'yith_shop_vendor',
            'term' => $current_user->user_login,
            //'posts_per_page' => 10,
            'orderby' => 'asc'
        );
        $the_query = new WP_Query($args);
        if ($the_query->have_posts()) : while ($the_query->have_posts()) :
            $the_query->the_post();
            $fields = get_fields();
            ?>
            <tr>
                <td class="col01">
                    <input id="product-select-id-217" data-product-id="217" type="checkbox"/>
                </td>
                <td class="col02" data-label="Product name">
                    <div class="thumb_container">
                        <?php
                        $current_post_id = $post->ID;
                        $thumbnail = get_the_post_thumbnail($current_post_id, '80x80');
                        preg_match('/src="(.*)" class/', $thumbnail, $link);
                        $thumbnail_url = $link[1];
                        if (!empty($link[1])): ?>
                            <img src="<?php echo $thumbnail_url; ?>"/>
                        <?php endif; ?>
                    </div>
                </td>
                <td class="col03">
                    <?php
                    $taxonomy = 'product_brand';
                    $current_brands = get_the_terms($current_post_id, $taxonomy);
                    ?>
                    <p>
                        <?php echo $post->post_title; ?>
                        <?php
                        foreach ($current_brands as $current_brand) {
                            echo '<span class="label">' . $current_brand->name . '</span > ';
                        }
                       ?>
                    </p>
                </td>
                <td class="col04" data-label="Stock">
                    <p>
                        <?php
                        $arr_size = array(
                            "XS" => "xs",
                            "S" => "s",
                            "M" => "m",
                            "L" => "l",
                            "XL" => "xl",
                            "XXL" => "xxl",
                            "One size" => "s_one_size",
                            "Oversize" => "s_oversize",
                            "XS/S" => "s_xs_s",
                            "S/M" => "s_s_m",
                            "M/L" => "s_m_l",
                            "L/XL" => "s_l_xl",
                            "5" => "s_5",
                            "6" => "s_6",
                            "7" => "s_7",
                            "8" => "s_8",
                            "9" => "s_9",
                            "10" => "s_10",
                            "11" => "s_11",
                            "12" => "s_12",
                        );

                        foreach ($arr_size as $field_descr => $field_key) {
                            $arr_sizeof = 0;
                            if ( false != ( $field_value = get_field($field_key))) {
                                echo '<span> ' . $field_descr . ':' . $arr_sizeof += sizeof($field_value) . '<span>';
                            } else {
                                //echo '<span class="size_is_null"> ' . $field_descr . ':' . ' 0</span>';
                            }
                        }
                        ?>
                    </p>
                </td>
                <td class="col05">
                    <a class="ajax_tb_manage_stock button-primary" data-product-id="<?php echo $current_post_id; ?>" href="#"> Manage Stock </a>
                </td>
                <td class="col06" data-label="Product status">
                    <?php echo my_get_product_status_str( get_field('product_status') ); ?>
                </td>
            </tr>
        <?php endwhile; else : ?>
            <p style="text-align: center;">There were no products.</p>
        <?php endif;
        wp_reset_postdata(); ?>
        </tbody>
    </table>
</main>
<div id="tb-manage-stock" style="display:none;">
    <div id="tb-manage-stock__modal__loading" class="modal__manage-stock__loading">Loading...</div>
    <div id="tb-manage-stock__modal" class="modal__manage-stock" style="display: none;">
        <form name="form_modal_manage_stock" id="form_modal_manage_stock" action="/?vendor-dashboard" method="post">
        <div class="modal__manage-stock__thumb-container alignleft">
            <img class="modal__manage-stock__thumb-container__thumbnail" alt="Product thumbnail" src="#" />
        </div>
        <div class="modal__manage-stock__input-container alignleft">
            <div class="clearfloat">
                    <div class="modal__manage-stock__input-container__type alignleft"></div>
                    <div class="modal__manage-stock__input-container__value alignright"></div>
            </div>
        </div>
        <div class="modal__manage-stock__btn"></div>
        </form>
    </div>
</div>


