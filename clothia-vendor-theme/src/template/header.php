<?php
$vendor_dashboard = new VendorDashboard();
?>
<!doctype html>
<html class="site no-js" lang="en-US">
<head>
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vendor Dashboard</title>
    <link rel='stylesheet' id='main.css'  href='<?php echo $vendor_dashboard->my_get_template_url() ?>/assets/css/main.css' type='text/css' media='all' />
    <script type='text/javascript' src='<?php echo $vendor_dashboard->my_get_template_url() ?>/assets/js/main.js'></script>
</head>
<body  data-current-page="<?php if(!is_page('vendor-login')) { echo get_current_page(); } ?>">