<header class="page__login__header">
    <div class="container">
        <div class="row">
            <div class="small-12 column">
                <div class="page__login__header__logo">
                    <a href="<?php bloginfo('wpurl'); ?>/" class="logo">
                        <img width="256" height="66"
                             src="<?php echo $vendor_dashboard->my_get_template_url() ?>/assets/svg/header/logo__login_page.svg"
                             alt="Clothia &#8211; Best new designers from Europe">
                    </a>
                </div>
                <div class="page__login__header__vendor-dashboard set_uppercase">
                    Vendor Dashboard
                </div>
            </div>
        </div>
    </div>
</header>
<main class="page__login__main">
    <div class="page__login__main__form">
        <form name="loginform" id="loginform" action="<?php bloginfo('wpurl'); ?>/wp-login.php" method="post">
            <p>
                <label class="set_uppercase">Email<br/>
                    <input type="text" name="log" id="user_login" class="input" value="" size="20" tabindex="10"
                           placeholder="john.doe@gmail.com"/></label>
            </p>
            <p>
                <label class="set_uppercase">Password<br/>
                    <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" tabindex="20"
                           placeholder="helloworld881"/></label>
            </p>
            <p class="page__login__main__form__forgot-passwd">
                Forgot your password.
                <a href="<?php echo get_option('home'); ?>/wp-login.php?action=lostpassword" title="Forgot your password. Click here!">Click here!</a>
            </p>
            <p class="submit">
                <input type="submit" name="wp-submit" id="wp-submit" class="button-primary set_uppercase" value="Log In"
                       tabindex="100"/>
                <input type="hidden" name="redirect_to" value="<?php bloginfo('wpurl'); ?>/<?php echo $vendor_dashboard->dashboard_product_page; ?>/?vendor-dashboard"/>

                <input type="hidden" name="testcookie" value="1"/>
            </p>
        </form>
    </div>
</main>