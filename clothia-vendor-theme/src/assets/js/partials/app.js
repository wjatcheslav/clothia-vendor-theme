// debug funcs
/* repeatString() returns a string which has been repeated a set number of times */
function repeatString(str, num) {
    out = '';
    for (var i = 0; i < num; i++) {
        out += str;
    }
    return out;
}

/*
dump() displays the contents of a variable like var_dump() does in PHP. dump() is
better than typeof, because it can distinguish between array, null and object.
Parameters:
  v:              The variable
  howDisplay:     "none", "body", "alert" (default)
  recursionLevel: Number of times the function has recursed when entering nested
                  objects or arrays. Each level of recursion adds extra space to the
                  output to indicate level. Set to 0 by default.
Return Value:
  A string of the variable's contents
Limitations:
  Can't pass an undefined variable to dump().
  dump() can't distinguish between int and float.
  dump() can't tell the original variable type of a member variable of an object.
  These limitations can't be fixed because these are *features* of JS. However, dump()
*/
function dump(v, howDisplay, recursionLevel) {
    //console.log('run dump() func');
    howDisplay = (typeof howDisplay === 'undefined') ? "alert" : howDisplay;
    recursionLevel = (typeof recursionLevel !== 'number') ? 0 : recursionLevel;
    var vType = typeof v;
    var out = vType;
    switch (vType) {
        case "number":
        /* there is absolutely no way in JS to distinguish 2 from 2.0
        so 'number' is the best that you can do. The following doesn't work:
        var er = /^[0-9]+$/;
        if (!isNaN(v) && v % 1 === 0 && er.test(3.0))
            out = 'int';*/
        case "boolean":
            out += ": " + v;
            break;
        case "string":
            out += "(" + v.length + '): "' + v + '"';
            break;
        case "object":
            //check if null
            if (v === null) {
                out = "null";

            }
            //If using jQuery: if ($.isArray(v))
            //If using IE: if (isArray(v))
            //this should work for all browsers according to the ECMAScript standard:
            else if (Object.prototype.toString.call(v) === '[object Array]') {
                out = 'array(' + v.length + '): {\n';
                for (var i = 0; i < v.length; i++) {
                    out += repeatString('   ', recursionLevel) + "   [" + i + "]:  " +
                        dump(v[i], "none", recursionLevel + 1) + "\n";
                }
                out += repeatString('   ', recursionLevel) + "}";
            }
            else { //if object
                sContents = "{\n";
                cnt = 0;
                for (var member in v) {
                    //No way to know the original data type of member, since JS
                    //always converts it to a string and no other way to parse objects.
                    sContents += repeatString('   ', recursionLevel) + "   " + member +
                        ":  " + dump(v[member], "none", recursionLevel + 1) + "\n";
                    cnt++;
                }
                sContents += repeatString('   ', recursionLevel) + "}";
                out += "(" + cnt + "): " + sContents;
            }
            break;
    }
    if (howDisplay == 'body') {
        var pre = document.createElement('pre');
        pre.innerHTML = out;
        document.body.appendChild(pre)
    }
    else if (howDisplay == 'alert') {
        alert(out);
    }
    return out;
}

// page: Dashboard Inventory
jQuery(document).ready(function ($) {

    $('#field_keyword').on('input');

    $('#field_keyword').on('input', function () {
        var keyword = $('#field_keyword').val();
        $.ajax(
            {
                type: 'post',
                dataType: 'json',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'get_product_list',
                    keyword: keyword
                },
                success: function (result) {
                    var current_page = $('body').attr('data-current-page');
                    switch (current_page) {
                        // case dashboard inventory {
                        case "dashboard-inventory":
                            $('#inventory_list').html('');
                            cnt = 0;
                            var current_product = '';
                            for (var current_product in result) {
                                var row = '';
                                row += '<tr>';
                                row += '<td class="col01">';
                                row += '<input id="product-select-id-' + result[cnt].ID + '" data-product-id="' + result[cnt].ID + '" type="checkbox"/>';
                                row += '</td>';
                                row += '<td class="col02" data-label="Product name">';
                                row += '<div class="thumb_container">';
                                row += '<img src="' + result[cnt].post_thumbnail + '" />';
                                row += '</div>';
                                row += '</td>';
                                row += '<td class="col03">';
                                row += '<p>';
                                row += result[cnt].post_title;
                                var cnt_brand = 0;
                                var current_brand = '';
                                for (var current_brand in result[cnt].brands) {
                                    row += '<span class="label">';
                                    row += result[cnt].brands[cnt_brand];
                                    row += '</span>';
                                    cnt_brand++;
                                }
                                row += '</p>';
                                row += '</td>';
                                row += '<td class="col04" data-label="Stock">';
                                row += '<p>';
                                var cnt_size = 0;
                                var current_size = '';
                                for (var current_size in result[cnt].isset_size) {
                                    row += '<span>';
                                    row += current_size + ':' + result[cnt].isset_size[current_size] + ' ';
                                    row += '</span>';
                                    cnt_size++;
                                }
                                row += '</p>';
                                row += '</td>';
                                row += '<td class="col05">';
                                row += '<a class="ajax_tb_manage_stock button-primary" data-product-id="' + result[cnt].ID + '"  href="#"> Manage Stock </a>';
                                row += '</td>';
                                row += '<td class="col06" data-label="Product status">';
                                row += result[cnt].product_status_str;
                                row += '</td>';
                                row += '</tr>';
                                $('#inventory_list').append(row);
                                cnt++;
                            }
                            break;
                        // } case dashboard inventory
                        // case dashboard product {
                        case "dashboard-product":
                            $('#product_list').html('');
                            cnt = 0;
                            var current_product = '';
                            for (var current_product in result) {
                                var row = '';
                                row += '<tr>';
                                row += '<td class="col01">';
                                row += '<input id="product-select-id-' + result[cnt].ID + '" data-product-id="' + result[cnt].ID + '" type="checkbox"/>';
                                row += '</td>';
                                row += '<td class="col02" data-label="Product name">';
                                row += '<div class="thumb_container">';
                                row += '<img src="' + result[cnt].post_thumbnail + '" />';
                                row += '</div>';
                                row += '</td>';
                                row += '<td class="col03">';
                                row += '<p>';
                                row += result[cnt].post_title;
                                row += '<span class="descr">';
                                row += result[cnt].post_content;
                                row += '</span>';
                                row += '</p>';
                                row += '</td>';
                                row += '<td class="col04" data-label="Stock">';
                                row += '<table>';
                                row += '<tr>';
                                //row += '<td align="left" valign="top" class="width70"><p class="composition pending color_' + result[cnt].ID + '">Composition:</p></td>';
                                row += '<td align="left" valign="top" class="width70"><p class="composition pending">Composition:</p></td>';
                                //row += '<td align="left" valign="top"><p class="care pending color_' + result[cnt].ID + '">' + result[cnt].fields.composition + '</p></td>';
                                row += '<td align="left" valign="top"><p class="care pending">' + result[cnt].fields.composition + '</p></td>';
                                row += '</tr>';
                                row += '<tr>';
                                row += '<td align="left" valign="top" class="width70"><p class="composition">Care:</p></td>';
                                row += '<td align="left" valign="top"><p class="care">' + result[cnt].fields.care + '</p></td>';
                                row += '</tr>';
                                row += '</table>';
                                row += '</td>';
                                row += '<td class="col05 last-note pending">';
                                row += result[cnt].fields.service_information;
                                row += '</td>';
                                row += '<td class="col06" data-label="Product status">';
                                row += result[cnt].product_status_str;
                                row += '</td>';
                                row += '<td class="col07">';
                                //row += '<a class="ajax_tb_product button-primary bg_' + result[cnt].ID + '" data-product-id="' + result[cnt].ID + '" href="#"> More details </a>';
                                row += '<a class="ajax_tb_product button-primary" data-product-id="' + result[cnt].ID + '" href="#"> More details </a>';
                                row += '</td>';
                                row += '</tr>';
                                // set status color
                                $('#product_list').append(row);
                                /*
                                if (result[cnt].fields.product_status == 0) {
                                    $('.bg_' + result[cnt].ID).css(
                                        {
                                            'background-color': '#000'
                                        });
                                    $('.color_' + result[cnt].ID).css(
                                        {
                                            'color': '#f5a623'
                                        });
                                }
                                if (result[cnt].fields.product_status == 3) {
                                    $('.bg_' + result[cnt].ID).css(
                                        {
                                            'background-color': '#000'
                                        });
                                    $('.color_' + result[cnt].ID).css(
                                        {
                                            'color': '#417505'
                                        });
                                }
                                if (result[cnt].fields.product_status == 4) {
                                    $('.bg_' + result[cnt].ID).css(
                                        {
                                            'background-color': '#d0011b'
                                        });
                                    $('.color_' + result[cnt].ID).css(
                                        {
                                            'color': '#d0011b'
                                        });
                                }*/
                                cnt++;
                            }
                            break;
                        // } case dashboard product
                    }
                }
            });
    });

    // ajax_product_list {
    $('body').on('click', '.ajax_tb_manage_stock', function () {
        var product_id = $(this).attr("data-product-id");

        $.ajax(
            {
                type: 'post',
                dataType: 'json',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'get_product',
                    product_id: product_id
                },
                success: function (result) {
                    tb_show('Manage Stock', '#TB_inline?inlineId=tb-manage-stock&ampleft=0', true);
                    $('#TB_load').hide();
                    $('#TB_window').show();
                    $('.modal__manage-stock__input-container__type').html('');
                    $('.modal__manage-stock__input-container__value').html('');
                    $('.modal__manage-stock__btn').html('');
                    $('#TB_closeAjaxWindow').html('<a id="TB_closeWindowButton" class="tb-close-icon alignright" href="#" onclick="tb_remove();"></a>');
                    $('img.modal__manage-stock__thumb-container__thumbnail').attr('src', result.post_thumbnail);
                    var cnt = 0;
                    for (var current_size_type in result.isset_size) {
                        $('.modal__manage-stock__input-container__type').append('<p><input type="text" name="field_' + current_size_type + '" ize="20" tabindex="10" value="' + current_size_type + '" disabled/></p>');
                        $('.modal__manage-stock__input-container__value').append('<p><input type="text" name="field_' + current_size_type + '" ize="20" tabindex="10" value="' + result.isset_size[current_size_type] + '" /></p>');
                        if (cnt == 0) {
                            $('.modal__manage-stock__input-container__type').children('p').prepend('<label class="set_uppercase">Size</label><br/>');
                            $('.modal__manage-stock__input-container__value').children('p').prepend('<label class="set_uppercase">Value</label><br/>');
                        }
                        console.log("key:" + current_size_type + " value:" + result.isset_size[current_size_type]);
                        cnt++;
                    }
                    $('.modal__manage-stock__btn').append('<a href="#" class="button-primary black alignleft" onclick="tb_remove();">Cansel</a>');
                    $('.modal__manage-stock__btn').append('<a href="#" class="button-primary black alignleft" onclick="tb_remove();">Save</a>');
                    set_tb_product_styles();
                    $('#tb-manage-stock__modal__loading').hide();
                    $('#tb-manage-stock__modal').show();
                }
            });
    });
    $('body').on('click', '.ajax_tb_product', function () {
        var product_id = $(this).attr("data-product-id");
        $.ajax(
            {
                type: 'post',
                dataType: 'json',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'get_product',
                    product_id: product_id
                },
                success: function (result) {
                    tb_show('<div class="alignleft">Manage Stock</div>', '#TB_inline?inlineId=tb-product&ampleft=0', true);
                    $('#TB_load').hide();
                    $('#TB_window').show();
                    $('.modal__product__input-container__descr').html('');
                    $('.modal__product__input-container__diment').html('');
                    $('.modal__product__btn').html('');
                    $('.modal__product__diment').html('');
                    $('#TB_closeAjaxWindow').html('<a id="TB_closeWindowButton" class="tb-close-icon alignright" href="#" onclick="tb_remove();"></a>');
                    $('img.modal__product__thumb-container__thumbnail').attr('src', result.post_thumbnail);
                    var regex = /(<([^>]+)>)/ig;
                    $('.modal__product__input-container__descr').append('<p><label class="set_uppercase">Post title</label><br /><input type="text" name="field_post_title" size="20" tabindex="10" value="' + result.post_title.replace(regex, "") + '" disabled/></p>');
                    $('.modal__product__input-container__descr').append('<p><label class="set_uppercase">Description</label><br /><textarea name="field_post_content" size="20" tabindex="30" disabled>' + result.post_content.replace(regex, "") + '</textarea></p>');
                    $('.modal__product__input-container__descr').append('<p><label class="set_uppercase">Composition</label><br /><input type="text" name="field_composition" size="20" tabindex="10" value="' + result.fields.composition.replace(regex, "") + '" disabled/></p>');
                    $('.modal__product__input-container__descr').append('<p><label class="set_uppercase">Care</label><br /><input type="text" name="field_care" size="20" tabindex="10" value="' + result.fields.care.replace(regex, "") + '" disabled/></p>');
                    var cnt = 0;
                    var current_value = '';
                    for (var current_size_type in result.isset_size) {
                        $('.modal__product__diment').append('<div class="clearfloat"><p class="modal__product__diment__type alignleft">' + current_size_type + '</p><input class="alignleft" type="text" name="field_post_title" size="20" tabindex="10" value="' + result.fields[current_size_type][0].label[0].post_title + ', ' + result.fields[current_size_type][0].value + '" /></div>');
                        if (cnt == 0) {
                            $('.modal__product__diment').children('div').prepend('<label class="set_uppercase">Product dimensions</label><br />');
                        }
                        cnt++;
                    }
                    $('.modal__product__btn').append('<a href="#" onclick="tb_remove();" class="button-primary white alignleft">Cansel</a>');
                    $('.modal__product__btn').append('<a href="#" onclick="tb_remove();" class="button-primary black alignleft">Save</a>');
                    set_tb_product_styles();
                    $('#tb-product__modal__loading').hide();
                    $('#tb-product__modal').show();
                }
            });
    });

});

function set_tb_product_styles() {
    jQuery('#TB_overlay').css(
        {
            'opacity': '0.09'
        });
    jQuery('#TB_window').css(
        {
            'top': '2%',
            'width': '664px',
            'border': 'none',
        });
    jQuery('#TB_ajaxContent').css(
        {
            'height': '100%',
            'width': '584px',
            'overflow': 'hidden',
            'padding': '40px'
        });

    jQuery('#TB_closeWindowButton>.screen-reader-text').css(
        {
            'display': 'none'
        });

    jQuery('#TB_title').css(
        {
            'text-transform': 'uppercase',
            'padding': '16px 0 18px 20px',
            'height': '16px',
            'background-color': '#000',
            'font-family': '"Proxima Nova", Helvetica Neue, Helvetica, Roboto, Arial, sans-serif',
            'font-size': '13px',
            'font-weight': '700',
            'color': '#fff',
        });
}