<?php
/* functions for not active theme mode { */
function my_get_template_dir()
{
    $my_theme = wp_get_theme('clothia-vendor-theme-build');
    $template = false;
    if ($my_theme->exists()) {
        $template = $my_theme->theme_root . '/' . $my_theme->template . '/';
    }
    return $template;
}

function my_get_js_dir()
{
    $js = false;
    if ($template = my_get_template_dir()) {
        $js = $template . 'assets/js/';
    }
    return $js;
}

function my_get_css_dir()
{
    $css = false;
    if ($template = my_get_template_dir()) {
        $css = $template . 'assets/css/';
    }
    return $css;
}

function my_get_img_dir()
{
    $img = false;
    if ($template = my_get_template_dir()) {
        $img = $template . 'assets/css/';
    }
    return $img;
}

/* } Functions for not active theme mode */

add_filter('jpeg_quality', create_function('', 'return 100;'));
add_theme_support('post-thumbnails');
add_image_size('blog_teaser', 200, 200, true);
add_image_size('blog_teaser_2x', 400, 400, true);
add_image_size('look_catalog', 570, 570, true);
add_image_size('look_catalog_2x', 1140, 1140, true);
add_image_size('shop_catalog_2x', 620, 620, false);
add_image_size('shop_single', 640, 640, false);
add_image_size('shop_single_2x', 1280, 1280, false);
add_image_size('shop_thumbnail_2x', 196, 196, false);
add_image_size('trimmed', 1024, 1024);
add_image_size('square', 720, 720);
add_image_size('80x80', 80, 80, false);
add_image_size('160x160', 160, 160, false);
add_image_size('1600x1600', 1600, 1600, false);

function my_get_product_status_str($num)
{
    if ($num == 0) {
        return '<p class="pending">Pending</p>';
    } elseif ($num == 1) {
        return '<p class="approved">Approved</p>';
    } elseif ($num == 2) {
        return '<p class="featured">Published</p>';
    } elseif ($num == 3) {
        return '<p class="featured">Featured</p>';
    } elseif ($num == 4) {
        return '<p class="out_of_stock">Out of stock</p>';
    } else {
        return 'Error';
    }
}

//add_action('init', 'init_thickbox_method');

//function init_thickbox_method() {
//add_thickbox();
//}

function is_user_role($role, $user_id = null)
{
    global $current_user;
    $user = is_numeric($user_id) ? get_userdata($user_id) : wp_get_current_user();
    if (!$user) {
        return false;
    }
    return in_array($role, (array)$user->roles);
}

//add_action('init', 'redirect_non_admin_user', 0);

function redirect_non_admin_user(){
    //if ( !is_user_role('administrator') && ( !is_user_role('yith_vendor')) && !is_ajax() ){
    if( is_user_role('yith_vendor') ) {
        print_r($_REQUEST['action']);
        die();
        /*
        if( isset($_REQUEST['action']) && ctrcmp($_REQUEST['action'],'modal_manage_stock' ) == 0) {
            die('ajax modal');
            do_action( 'wp_ajax_' . $_REQUEST['action'] );
            do_action( 'wp_ajax_nopriv_' . $_REQUEST['action'] );

        }*/
        //wp_redirect( site_url() );  exit;
    }
}


/*Woocommerce Account Redirect*/
function wpmu_woocommerce_account_redirect()
{
    if (!defined('DOING_AJAX')) {
        die('resp');
    }

    if (is_user_logged_in() && is_account_page() ) {
        wp_redirect( '/members/' . bp_core_get_username( bp_loggedin_user_id() ) );
        exit;
    }
}


//add_action( 'wp_redirect', 'my_page_template_redirect' );
function my_page_template_redirect()
{
    if( is_page( 'goodies' ) && ! is_user_logged_in() )
    {
        wp_redirect( home_url( '/signup/' ) );
        exit();
    }
    echo '#'.'redirect_hook';
    die();
}

add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $template ){
    global $wp_query;
    $GLOBALS['current_page'] = $wp_query->get_queried_object()->post_name;
    return $template;
}

function get_current_page( $echo =  false ) {
    if( !isset( $GLOBALS['current_page'] ) )
        return false;
    return $GLOBALS['current_page'];
}




