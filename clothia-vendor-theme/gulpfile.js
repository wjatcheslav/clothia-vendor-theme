'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    coffee = require('gulp-coffee'),
    addsrc = require('gulp-add-src'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/assets/js/',
        coffee: 'build/assets/coffee/',
	css: 'build/assets/css/',
        img: 'build/assets/img/',
	svg: 'build/assets/svg/',
        fonts: 'build/assets/fonts/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.php', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/assets/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
        coffee: 'src/assets/js/coffee/**/*.coffee',
	style: 'src/assets/style/main.scss',
        img: 'src/assets/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
	svg: 'src/assets/svg/**/*.*',
        fonts: 'src/assets/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/**/*.php',
        js: 'src/assets/js/**/*.*',
	coffee: 'src/assets/js/coffee/**/*.coffee',
        style: 'src/assets/style/**/*.scss',
        img: 'src/assets/img/**/*.*',
	svg: 'src/assets/svg/**/*.*',
        fonts: 'src/assets/fonts/**/*.*'
    },
    clean: './build'
};

/*
var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 3001,
    logPrefix: "Frontend_Vendor"
};
*/

gulp.task('html:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('coffee:build', function () {
  return gulp.src('path.src.coffee')   // start with the .coffee files in the project
	.pipe(coffee())                          // compiles coffee script
	//pipe(addsrc(path.src.js))           // we use addsrc to add our .js files to the mix
	.pipe(sourcemaps.init())
	.pipe(uglify())                          // we minify everything
	.pipe(sourcemaps.write())
	.pipe(gulp.dest(path.build.coffee))                // and write to dist
	.pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) //Выберем наши картинки
/*        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
*/
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('svg:build', function () {
    gulp.src(path.src.svg) //Выберем наши картинки
/*        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
*/
        .pipe(gulp.dest(path.build.svg)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'js:build',
    'coffee:build',
    'style:build',
    'fonts:build',
    'image:build',
    'svg:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.coffee], function(event, cb) {
        gulp.start('coffee:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.svg], function(event, cb) {
        gulp.start('svg:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

/*
gulp.task('webserver', function () {
    browserSync(config);
});
*/

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

//gulp.task('default', ['build', 'webserver', 'watch']);

gulp.task('default', ['build', 'watch']);

//Notifi

var gutil    = require('gulp-util');
var notifier = require('node-notifier');


/**
 * Fake the gulp-notfy functionality
 * to provide a consistent interface
 * for non-stream notifications
 *
 * @param message
 */
/*
        gutil.log(
                gutil.colors.cyan('gulp-notifier'),
                '[' + gutil.colors.blue('Gulp notification') + ']',
                gutil.colors.green(message)
        );

        notifier.notify({
                title: 'Gulp notification',
                message: message,
                onLast: true
        });
*/
